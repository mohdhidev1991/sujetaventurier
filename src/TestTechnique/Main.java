package TestTechnique;

public class Main {

	public static void main(String[] args) {
		
		// TESTER  L'aventure 1 avec (3,0 SSSSEEEEEENN)

        HeroAdventure aventure1 = new HeroAdventure("carte_v2.txt",3,0);
        aventure1.displayMap();
        // Déplacements du personnage
        String movements = "SSSSEEEEEENN";
        aventure1.moveCharacter(movements);
        // Afficher les coordonnées finales du personnage
        aventure1.printCoordinates();
        
        
        
       // TESTER  L'aventure 2 avec (6,7 OONOOOSSO)

        HeroAdventure aventure2 = new HeroAdventure("carte_v2.txt",6,7);
        aventure2.displayMap();
        // Déplacements du personnage
        String movements2 = "OONOOOSSO";
        aventure2.moveCharacter(movements);
        // Afficher les coordonnées finales du personnage
        aventure2.printCoordinates();
    }

}
