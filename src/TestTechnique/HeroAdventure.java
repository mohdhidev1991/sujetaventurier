package TestTechnique;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class HeroAdventure {
    char[][] map;
    int currentX;
    int currentY;

    public HeroAdventure(String filePath,int intialX,int intialY) {
        loadMap(filePath,intialX,intialY);
    }

    public void loadMap(String filePath,int intialX , int intialY) {
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            int row = 0;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                if (row == 0) {
                    
                        currentX = intialX ;
                        currentY = intialY ;
                   
                } else {
                    // Les lignes suivantes contiennent la carte
                    if (map == null) {
                        map = new char[1][line.length()];
                    } else if (row > map.length) {
                        // Agrandir le tableau
                        char[][] newMap = new char[row][line.length()];
                        System.arraycopy(map, 0, newMap, 0, map.length);
                        map = newMap;
                    }
                    map[row - 1] = line.toCharArray();
                }
                row++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void printCoordinates() {
        System.out.println("Coordonnées finales du personnage : (" + currentX + "," + currentY + ")");
    }




    // Méthode pour afficher la carte
    public void displayMap() {
        for (char[] row : map) {
            System.out.println(row);
        }
    }

    // Méthode pour déplacer le personnage
    public void moveCharacter(String movements) {
        for (char direction : movements.toCharArray()) {
           int newX = currentX;
           int newY = currentY;

            switch (direction) {
                case 'N':
                    newY--;
                    break;
                case 'S':
                    newY++;
                    break;
                case 'E':
                    newX++;
                    break;
                case 'O':
                    newX--;
                    break;
                default:
                    // Ignorer les directions non valides
            }

            
            currentX = newX;
            currentY = newY;
            
        }
    }


    

    

}

